# Copyright (c) 2016, Erwin Goossen <erwin@navelpluisje.nl>
# Permission to use, copy, modify, and/or distribute this software for any 
# purpose with or without fee is hereby granted, provided that the above 
# copyright notice and this permission notice appear in all copies.
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR 
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
# PERFORMANCE OF THIS SOFTWARE.

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome import service as chrome_service

from PIL import Image

# import selenium.webdriver.chrome.service as service
import yaml
import os
import time


# Get the config file content
# Returns the content or False on error
def get_config_file():
    try:
        config_file = open('config.yml')
    except SystemExit:
        return False
    finally:
        return yaml.load(config_file)


# Check if a dir exists and create one if it does not
# returns the currentDir afterworth
def check_dir(directory):
    curdir = os.path.dirname(os.path.realpath(__file__))
    curdir += directory
    if not os.path.exists(curdir):
        os.makedirs(curdir)
    return curdir


# Create the emulator object
def set_up_emulator(width, height, pixelratio):
    return {"deviceMetrics": {"width": width, "height": height, "pixelRatio": pixelratio},
            "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
            }


# Create and return the driver
def set_driver(dev):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("mobileEmulation",
                                           set_up_emulator(dev['width'], dev['height'], dev['pixelratio']))

    the_driver = webdriver.Remote(service.service_url, desired_capabilities=chrome_options.to_capabilities())

    the_driver.set_window_size(device['width'] * 1.5, device['height'] * 1.5)
    return the_driver


# Login to the app
def login(the_driver):
    # First open our login page
    the_driver.get(config['login']['url'])
    # Don't do a thing before the submit button is available
    # If this one timeouts we quit the the_driver
    try:
        WebDriverWait(the_driver, 15).until(ec.visibility_of_element_located((By.ID, config['login']['submit'])))
    except Exception:
        the_driver.quit()
    finally:
        # Now fill in the foprm and submit the login
        the_driver.find_element_by_id(config['login']['nameid']).send_keys(config['login']['name'], Keys.TAB)
        the_driver.find_element_by_id(config['login']['passwordid']).send_keys(config['login']['password'], Keys.ENTER)
        the_driver.execute_script('document.getElementById("login-button").click()')


service = chrome_service.Service('driver/chromedriver')
service.start()

config = get_config_file()
if not config:
    exit('Config file could not be loaded. Please check its excistence!')


for screen in config['screens']:
    for device in screen['devices']:
        directory = '/' + screen['name'] + '/' + device['name']
        directory = check_dir(directory)

        driver = set_driver(device)

        if config.has_key('login'):
            login(driver)

        for shot in config['uri']:
            driver.get(shot['url'])
            try:
                if config['global']['visibletype'] == 'class':
                    WebDriverWait(driver, 15).until(ec.visibility_of(driver.find_element_by_class_name(config['global']['visible'])))
                elif config['global']['visibletype'] == 'id':
                    WebDriverWait(driver, 15).until(ec.visibility_of(driver.find_element_by_id(config['global']['visible'])))
            except Exception:
                driver.quit()
            finally:
                time.sleep(3)
                driver.save_screenshot('tmp.png')
                img = Image.open('tmp.png')
                img.crop((
                    0,
                    0,
                    device['width'] * config['global']['pixelrate'],
                    device['height'] * config['global']['pixelrate'])).save(directory + '/' + shot['name'] + '.png')

        driver.quit()
