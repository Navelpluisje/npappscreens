#  npAppScreens

npAppScreens is a small Python script for generating screenshots for your app for several devices.
It uses a yaml config file for setting the devices with screen dimensions and the pages to take a screenshot from.

## Dependencies

It does run on Python 2.7. Other versions are not tested yet.

Other dependencies are:

* selenium `pip install selenium`
* Pillow, for image manipulation `pip install Pillow`
* PyYAML, for handling the config file `pip install PyYAML`

## The config file

The config file consists of 4 main items:

### global

All the fields are mandatory

    global:
      pixelrate: 2
      visible: 'screenfiller'
      visibletype: 'class'

* **pixelrate**: Used for pixel compensation on retina displays. Set this one to `2` for retina and `1` otherwise.
* **visible**: The class or id of the main container which appears in every window to check if the page is loaded
* **visibletype**: Set to `class` if visible is a class or to `id` if it is an id

### screens

Create a list of screen or devices to take screenshots for

    screens:
      - name: android
        devices:
          - name: phone
            width: 360
            height: 640
            pixelratio: 3
            
This part contains 2 levels. 

1. **brand**: For instance `android` or `apple`. This is also used to create the file structure. It contains:
    * **name**: brandname, used for file structure. There is always one needed
    * **devices**: read the next bullet
2. **devices**: One `brand` can contain multiple device. They contain:
    * **name**: name of the device. Used for the file structure
    * **width**: screen width of the device for creating the emulator
    * **height**: screen height of the device for creating the emulator
    * **pixelratio**: pixelratio of the device for creating the emulator
    
### uri

List of pages to take screenshots of

    uri:
      - name: start
        url: 'http://mijn.justlease.nl/#start'
      - name: bundelstatus
        url: 'http://mijn.justlease.nl/#bundelstatus'
        
* **name**: Name of the page/screen. Thsi is used as the nam eof the screenshot
* **url**: The url to get the screenshot of

### login

If your website has a login, include this part. This will make this script to login first

    login:
      url: 'http://accmijn.justlease.nl/#login'
      name: username
      password: password
      nameid: 'login-email'
      passwordid: 'login-password'
      submit: 'login-button'
      
* **url**: The url of the login page
* **name**: The login name
* **password**: The password
* **nameid**: The id of the username field
* **passwordid**: The id of the password field
* **submit**: The id of the submit button